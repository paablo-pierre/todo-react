/*
 * Types
 */
export const Types = {
    ADD_REQUEST: 'favorites/ADD_REQUEST',
    ADD_SUCCESS: 'favorites/ADD_SUCCESS',
    ADD_FAILURE: 'favorites/ADD_FAILURE',
};

/*
 * REDUCER
 */
const INITIAL_STATE = {
    loading: false,
    data: [],
    error: null,
};

export default function favorites(state = INITIAL_STATE, action) {
    switch(action.type) {
        case Types.ADD_REQUEST:
            return { ...state, loading: true};
        case Types.ADD_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                data: [ ...state.data, action.payload.data],
            };
        case Types.ADD_FAILURE :
            return {
                ...state, loading: false, error: action.payload.error,
            };
        default:
            return state;
    }

}

/*
 * ACTIONS
 */

export const Creators = {
    addFavoriteRequest: repository => ({
        type: Types.ADD_REQUEST,
        payload: { repository },
    }),

    addFavoriteSuccess: data => ({
        type: Types.ADD_SUCCESS,
        payload: { data },
    }),

    addFavoriteFailure: error => ({
        type: Types.ADD_FAILURE,
        payload: { error },
    })
};

/*
 * request será chamada pelo main.js
 * saga ouve o addFavoriteRequest
 * Request -> SAGA -> Chamada api -> Success
 */

/*
    * define-se um estado inicial
    * este passado por parametro função
    * junto com a action
    * captura-se o action.type e aí é executada a ação
    *
 */


/*
    * Action.data é recebido do requestSuccess
    * após receber os dados da API
 */
