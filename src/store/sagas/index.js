import { all, takeLatest } from 'redux-saga/effects';

import { addFavorite } from './favorites';
import { Types as FavoriteTypes} from '../ducks/favorites';
/*
 * primeiro parametro passado no takeLatest é
 * o que ele vai ouvir do input (REQUEST)
 * segundo parâmetro é a função que ele vai ouvir
 * do favorite na pasta sagas
 */

export default function* rootSaga() {
    yield all([
        takeLatest(FavoriteTypes.ADD_REQUEST, addFavorite)
    ]);
}