import React, { Component } from 'react';

import { Provider } from 'react-redux';

import './config/reactotron'; //importante, deve ser importado antes do store;
import store from './store';

import Routes from './routes';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Routes/>
            </Provider>
        );
    }
}

export default App;
